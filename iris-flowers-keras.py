import numpy as np

import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split
from sklearn.datasets import load_iris # import load_iris function from datasets module

from tensorflow.keras.models import Sequential, Model
from tensorflow.keras.utils  import to_categorical 
from tensorflow.keras.layers import Dense, Dropout, Flatten
from tensorflow.keras        import optimizers

## -- Load Iris dataset using sklearn (save "bunch" object containing iris dataset and its attributes) -- ##
iris = load_iris()
X = iris.data
y = iris.target

## -- Change the labels from categorical to one-hot encoding -- ##
# The output neurons of the NN will be trained to match the one_hot encoded array
# Example: category label 0 out of 3 labels becomes [1 0 0];
# Example: category label 1 out of 3 labels becomes [0 1 0];
# HINT: use: "to_categorical" to redifine the one hot encoded target y_one_hot using the original y.
# for i in range(len(y)):
#     if y[i] == 0:
#         y[i] = 1
#     else:
#         y[i] = 0
y_one_hot = to_categorical(y)

## -- Split the dataset for training, validation, and test -- ##
train_and_valid_X, test_X, train_and_valid_y, test_y  = train_test_split( X, y_one_hot, test_size=0.1 )
train_X, valid_X, train_y, valid_y = train_test_split( train_and_valid_X, train_and_valid_y, test_size=0.2 )


# define the neural network
def baseline_model():
    nb_nurons        = 8
    nb_Classes       = 3 # HINT:  <---- look here
    input_dimensions = 4
    learning_rate    = 0.01
    
    # create model
    model = Sequential()
    model.add(Dense(nb_nurons, input_dim=input_dimensions, activation='relu'))
    model.add(Dense(nb_Classes, activation='softmax')) # HINT: a 'softmax' activation will output a probability distribution over the output dimensions
    
    # Compile model
    opt = optimizers.RMSprop(lr=learning_rate)
    model.compile(loss='categorical_crossentropy', optimizer=opt, metrics=['accuracy']) # HINT: a 'binary_crossentropy' is only useful for at most 2 labels, look for another suitable loss function in Keras
    
    return model


def find_correct_and_incorrect_labels(model, test_X, test_y):
    predicted_classes = model.predict(test_X) # Computes for every input in the test dataset a probability distribution over the categories
    predicted_classes = np.argmax(predicted_classes, axis=1) # HINT: choose the prediction with the highest probability, np.argmax( ..... , axis=1 )
   
    correctIndex   = np.where( predicted_classes == np.argmax(test_y, axis=1) )[0] # HINT: replace test_y by np.argmax(test_y,axis=1)
    incorrectIndex = np.where( predicted_classes != np.argmax(test_y, axis=1) )[0] # HINT: replace test_y by np.argmax(test_y,axis=1)
   
    print("Found %d correct labels using the model"   % len(correctIndex))
    print("Found %d incorrect labels using the model" % len(incorrectIndex))


def plot_train_performance(trained_model): # !!!!! Do not change this function !!!!!
    accuracy     = trained_model.history['accuracy']
    val_accuracy = trained_model.history['val_accuracy']
    loss         = trained_model.history['loss']
    val_loss     = trained_model.history['val_loss']
    epochs       = range(len(accuracy))

    f1 = plt.figure(1)
    plt.subplot(1,2,1)
    plt.axis((0,len(epochs),0,1.2))
    plt.plot(epochs, accuracy, 'b', label='Training accuracy')
    plt.plot(epochs, val_accuracy, 'r', label='valid accuracy')
    plt.title('Training and valid accuracy')
    plt.legend()

    plt.subplot(1,2,2)
    plt.axis((0,len(epochs),0,1.2))
    plt.plot(epochs, loss, 'b', label='Training loss')
    plt.plot(epochs, val_loss, 'r', label='valid loss')
    plt.title('Training and valid loss')
    plt.legend()
    plt.show()
    plt.pause(0.001)


## -- Initialize model -- ##
model = baseline_model()

## -- Test the performmance of the untrained model over the test dataset -- ##
find_correct_and_incorrect_labels(model, test_X, test_y)

## -- Train the model -- ##
print('\nTraining started ...')
trained_model = model.fit(train_X, train_y, batch_size=8,epochs=30,verbose=1,validation_data=(valid_X, valid_y))
print('Training finished. \n')

## -- Test the performmance of the trained model over the test dataset -- ##
find_correct_and_incorrect_labels(model, test_X, test_y)

## -- Plot performance over training episodes -- ##
plot_train_performance(trained_model)
