import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
 

class Perceptron: 

	def __init__(self, inputs, targets, name='default neuron', learning_rate=0.1, iterations=20, threshold=0.5, hidden_layer_size=4):
		self.name = name
		self.learning_rate = learning_rate
		self.iterations = iterations
		self.threshold = threshold
		self.hidden_layer_size = hidden_layer_size

		# find out if matrix or single number or a matrix
		# if a single number, np.shape would only have the row
		if np.ndim(inputs) > 1:
			self.input_columns = np.shape(inputs)[1]
		else: 
			self.input_columns = 1
		
		if np.ndim(targets) > 1:
			self.target_columns = np.shape(targets)[1]
		else:
			self.target_columns = 1
		
		self.input_rows = np.shape(inputs)[0]
		
		# initialize weights 
		self.weights_hidden = np.random.rand( self.input_columns, self.hidden_layer_size )
		self.weights_output = np.random.rand( self.hidden_layer_size, self.target_columns )

		# initialize biases to each of the row columns
		self.bias_hidden = -np.ones( (self.input_rows, self.hidden_layer_size) )
		self.bias_output = -np.ones( (self.input_rows, self.target_columns) )
		
		
	def train(self, inputs, targets):
				
		for n in range(self.iterations):
			
			outputs = self.predict(inputs)
			
			# calculate deltas for output and hidden layers
			delta_o = (outputs - targets) * sigmoid_prime(outputs)
			delta_h =  sigmoid_prime(self.hidden) * np.dot(delta_o, np.transpose(self.weights_output))
			
			update_weights_hidden = self.learning_rate * np.dot(np.transpose(inputs), delta_h)
			update_weights_output = self.learning_rate * np.dot(np.transpose(self.hidden), delta_o)

			self.weights_hidden -= update_weights_hidden
			self.weights_output -= update_weights_output
			self.bias_hidden -= delta_h
			self.bias_output -= delta_o

			if n == self.iterations-1:
				self.accuracy(inputs, targets)
			
	
	def predict(self, inputs, is_final=False):

		self.hidden = np.dot(inputs, self.weights_hidden) + self.bias_hidden
		self.hidden = sigmoid(self.hidden)

		outputs = np.dot(self.hidden, self.weights_output) + self.bias_output
		outputs = sigmoid(outputs)

		return np.where(outputs > self.threshold, 1, 0) if is_final else outputs



	def accuracy(self, inputs, targets):

		predictions = self.predict(inputs, is_final=True)
		print(self.name)
		total = len(targets)
		correct = 0

		for t, p in zip(targets, predictions):
			if t == p:
				correct += 1


		print('no of correct predictions: ', correct)
		print('no of targets: ', total)
		print("Accuracy is :", (correct/total))
		print("=========================================")
	
def sigmoid(x):
	return 1.0/(1.0 + np.exp(-x))
		
		
def sigmoid_prime(x):
	return x * (1.0 - x)




if __name__ == '__main__':
	
	data_frame = pd.read_csv('https://archive.ics.uci.edu/ml/machine-learning-databases/iris/iris.data', header=None)
	inputs = data_frame.iloc[:, [0,1,2,3]].values
	labels = data_frame.iloc[:, 4].values

	targets = np.where(labels == 'Iris-setosa', 1, 0)
	targets = targets.reshape(-1, 1)
	neuron = Perceptron(inputs, targets, name="Setosa Perceptron", learning_rate=0.01)
	neuron.train(inputs, targets)

	targets = np.where(labels == 'Iris-versicolor', 1, 0)
	targets = targets.reshape(-1, 1)
	neuron = Perceptron(inputs, targets, name="Versicolor Perceptron", learning_rate=0.01)
	neuron.train(inputs, targets)

	targets = np.where(labels == 'Iris-virginica', 1, 0)
	targets = targets.reshape(-1, 1)
	neuron = Perceptron(inputs, targets, name="Virginica Perceptron", learning_rate=0.01)
	neuron.train(inputs, targets)


