import numpy as np


class Perceptron: 

	def __init__(self, learning_rate=0.1, iterations=20, threshold=0):
		self.learning_rate = learning_rate
		self.iterations = iterations
		self.threshold = threshold
		
		
	def train(self, inputs, targets):
		# find out if matrix or single number or a matrix
		# if a single number, np.shape would only have the row
		if np.ndim(inputs) > 1:
			self.input_columns = np.shape(inputs)[1]
		else: 
			self.input_columns = 1
		
		if np.ndim(targets) > 1:
			self.target_columns = np.shape(targets)[1]
		else:
			self.target_columns = 1
		
		self.input_rows = np.shape(inputs)[0]
		
		# initialize weights 
		self.weights = np.random.rand( self.input_columns+1,self.target_columns )
		
		# add a bias to the input, an entire column
		inputs = np.concatenate(( inputs, -np.ones((self.input_rows,1)) ), axis=1)
		
		for n in range(self.iterations):
			
			outputs = self.predict(inputs)
			self.weights += self.learning_rate * np.dot( np.transpose(inputs), targets - outputs  )
			print("Iteration: ", n+1)
			print("Weights: ")
			print(self.weights)
			activations = self.predict(inputs)
			print("Outputs are: ")
			print(activations)
			
	
	def predict(self, inputs):
		
		outputs = np.dot(inputs, self.weights)
		
		return np.where(outputs > self.threshold, 1, 0)
		


if __name__ == '__main__':
	
	inputs = np.array([ [0,0], [0,1], [1,0], [1,1] ])
	outputs = np.array([ [0], [0], [0], [1] ])
	
	neuron = Perceptron(learning_rate=0.01, iterations=50)
	
	neuron.train(inputs, outputs)
	
	#print("Final Prediction after training session: ")
	#predicts = neuron.predict(inputs)
	#print(predicts)
	
	
	
	
