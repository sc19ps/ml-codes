import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

class Perceptron: 

	def __init__(self, name='default neuron', learning_rate=0.1, iterations=20, threshold=0):
		self.name = name
		self.learning_rate = learning_rate
		self.iterations = iterations
		self.threshold = threshold
		
		
	def train(self, inputs, targets):
		# find out if matrix or single number or a matrix
		# if a single number, np.shape would only have the row
		if np.ndim(inputs) > 1:
			self.input_columns = np.shape(inputs)[1]
		else: 
			self.input_columns = 1
		
		if np.ndim(targets) > 1:
			self.target_columns = np.shape(targets)[1]
		else:
			self.target_columns = 1
		
		self.input_rows = np.shape(inputs)[0]
		
		# initialize weights 
		self.weights = np.random.rand( self.input_columns, self.target_columns )

		# add a bias to the input
		self.bias = -np.ones((self.input_rows, self.target_columns))
				
		for n in range(self.iterations):
			
			outputs = self.predict(inputs)
			self.weights += self.learning_rate * np.dot( np.transpose(inputs), targets - outputs  )
			self.bias += self.learning_rate * (targets - outputs)

			if n == self.iterations-1:
				self.accuracy(targets, outputs)
			
	
	def predict(self, inputs):

		outputs = np.dot(inputs, self.weights) + self.bias
		
		return np.where(outputs > self.threshold, 1, 0)


	def accuracy(self, targets, predictions):

		total = len(targets)
		correct = 0

		for t, p in zip(targets, predictions):
			if t == p:
				correct += 1

		print("Accuracy is :", (correct/total))

	
	
def scatter_plot(a, b, c):
		
	plt.scatter(a[:, 2], a[:, 3], color='red', marker='o', label='Iris Setosa')
	plt.scatter(b[:, 2], b[:, 3], color='blue', marker='x', label='Iris Versicolor')
	plt.scatter(b[:, 2], b[:, 3], color='green', marker='v', label='Iris Virginica')

	plt.xlabel('sepal length')
	plt.ylabel('petal length')
	plt.legend(loc='upper left')
	
	plt.show()

		


if __name__ == '__main__':
	
	data_frame = pd.read_csv('https://archive.ics.uci.edu/ml/machine-learning-databases/iris/iris.data', header=None)
	inputs = data_frame.iloc[:, [0,1,2,3]].values
	labels = data_frame.iloc[:, 4].values
	# scatter_plot(data_frame.iloc[0:50].values, data_frame.iloc[50:100].values, data_frame.iloc[100:150].values)
	
	
	neuron0 = Perceptron(name='Setosa neuron', learning_rate=0.01, iterations=20)
	targets = np.where(labels == 'Iris-setosa', 1, 0)
	targets = targets.reshape(-1, 1)
	neuron0.train(inputs, targets)
	
	neuron1 = Perceptron(name='Virginica neuron', learning_rate=0.01, iterations=100)
	targets = np.where(labels =='Iris-virginica', 1, 0)
	targets = targets.reshape(-1, 1)
	neuron1.train(inputs, targets)

	neuron2 = Perceptron(name = 'Versicolor neuron', learning_rate=0.01, iterations=100)
	targets = np.where(labels =='Iris-versicolor', 1, 0)
	targets = targets.reshape(-1, 1)
	neuron2.train(inputs, targets)





